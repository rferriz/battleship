#!/usr/bin/env php
<?php

require 'vendor/autoload.php';
require  'src/parser.php';

$args = parseArguments();

$human = array_key_exists('human', $args);
$ai_level = array_key_exists('computer', $args) ? $args['computer'] : null;
$hostServer = array_key_exists('server', $args) ? $args['server'] : null;

$showUsage = false;

if ($hostServer === null) {
    echo \Bart\EscapeColors::fg_color('red', 'Server is mandatory') . PHP_EOL;
    $showUsage = true;
}

if ($human && $ai_level !== null) {
    echo 'ERROR: Incompatible options human and computer' . PHP_EOL . PHP_EOL;
    $showUsage = true;
}

if (!$human && $ai_level === null) {
    echo 'ERROR: You must specify human or computer level.' . PHP_EOL . PHP_EOL;
    $showUsage = true;
}

if (array_key_exists('help', $args) || $showUsage) {
    echo 'BattleShip ConsoleClient v1.0' . PHP_EOL . PHP_EOL;
    echo 'Usage: php client.php --server hostname:port [--computer ai-level]|[--human]' . PHP_EOL;
    echo '  --computer    Start a Artificial Intelligence computer. Levels: easy, medium, hard' . PHP_EOL;
    echo '  --human       Start a game as human' . PHP_EOL;
    echo '  --server      Connect to the server. Example: --server 127.0.0.1:21080' . PHP_EOL;
    echo '  --help        Print this help screen' . PHP_EOL . PHP_EOL;
    exit(0);
}


use Monolog\Logger;

$logger = new Logger('client');
$handler = new \Monolog\Handler\StreamHandler('php://stdout', Logger::ERROR);
$logger->pushHandler($handler);

$server = new \GameHouse\BattleShip\Client\GameClient($logger);
$server->start($hostServer, $ai_level);
