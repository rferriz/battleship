#!/usr/bin/env php
<?php

require 'vendor/autoload.php';
require 'src/parser.php';

$args = parseArguments();

$port = array_key_exists('port', $args) ? intval($args['port']) : 21080;
$showUsage = false;

if (array_key_exists('help', $args) || $showUsage) {
    echo 'BattleShip ConsoleServer v1.0' . PHP_EOL . PHP_EOL;
    echo 'Usage: php server.php [--port port]' . PHP_EOL;
    echo '  --port        Port to listen (default 21080)' . PHP_EOL;
    echo '  --help        Print this help screen' . PHP_EOL . PHP_EOL;
    exit(0);
}

use Monolog\Logger;

$logger = new Logger('server');
$handler = new \Monolog\Handler\StreamHandler('php://stdout', Logger::DEBUG);
$logger->pushHandler($handler);

$server = new \GameHouse\BattleShip\Server\GameServer($logger);
$server->start($port);


