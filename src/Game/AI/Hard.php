<?php

namespace GameHouse\BattleShip\Game\AI;

use GameHouse\BattleShip\Game\Board;
use GameHouse\BattleShip\Game\BoardCell;
use GameHouse\BattleShip\Game\PlayerInterface;
use GameHouse\BattleShip\Game\Position;
use GameHouse\BattleShip\Game\Ship;

class Hard extends Medium
{
    public function setup(PlayerInterface $player)
    {
        parent::setup($player);
        $this->shipBorder = mt_rand(0, 3);
    }

    # TODO On sunk, recreate candidates adjusting new minimum size

}