<?php

namespace GameHouse\BattleShip\Game\AI;

use GameHouse\BattleShip\Game\PlayerInterface;
use GameHouse\BattleShip\Game\Position;
use GameHouse\BattleShip\Game\Ship;
use GameHouse\BattleShip\Game\ShipsLibrary;

class Context
{
    /** @var PlayerInterface */
    private $player;
    private $remaining = [];
    private $hit = [];
    private $sunk = [];
    /** @var array|Position[] */
    private $random = [];

    public function __construct(PlayerInterface $player)
    {
        $this->player = $player;

        $this->remaining = [];
    }

    public function initialize(ShipsLibrary $library)
    {
        foreach ($library->getShips() as $ship) {
            $this->remaining[] = [
                'ship' => $ship,
                'hits' => []
            ];
        }
    }

    public function getHit()
    {
        return $this->hit;
    }

    public function shipHit($name, Position $position)
    {
        // If in remaining, move to hit
        $remainIndex = null;
        foreach ($this->remaining as $key => $value) {
            /** @var Ship $ship */
            $ship = $value['ship'];
            if ($ship->getName() === $name) {
                $remainIndex = $key;
            }
        }
        if ($remainIndex !== null) {
            $this->hit[] = array_merge($this->remaining[$remainIndex], ['hits' => []]);
            unset($this->remaining[$remainIndex]);
        }

        // Add reported position
        $hitIndex = null;
        foreach ($this->hit as $key => $value) {
            /** @var Ship $ship */
            $ship = $value['ship'];
            if ($ship->getName() === $name) {
                $hitIndex = $key;
            }
        }
        $this->hit[$hitIndex]['hits'][] = $position;
    }

    public function shipSink($name)
    {
        $hitIndex = null;
        foreach ($this->hit as $key => $value) {
            /** @var Ship $ship */
            $ship = $value['ship'];
            if ($ship->getName() === $name) {
                $hitIndex = $key;
            }
        }

        $this->sunk[] = $this->hit[$hitIndex];
        unset($this->hit[$hitIndex]);

    }

    public function setRandom(array $random)
    {
        $this->random = $random;
    }

    public function getRandom()
    {
        return $this->random;
    }

    public function removeFromRandom(Position $position)
    {
        foreach($this->random as $k => $v) {
            if ($v instanceof Position) {
                if ($v->asString() === $position->asString()) {
                    unset($this->random[$k]);
                }
            }
        }
    }
}