<?php

namespace GameHouse\BattleShip\Game\AI;

class Factory
{
    static function create($level)
    {
        $level = mb_strtolower($level);

        switch ($level) {
            case 'easy':
                return new Easy();
            case 'medium':
                return new Medium();
            case 'hard':
                return new Hard();

            default:
                return new Easy();
        }
    }
}