<?php

namespace GameHouse\BattleShip\Game\AI;

use GameHouse\BattleShip\Game\Board;
use GameHouse\BattleShip\Game\PlayerInterface;
use GameHouse\BattleShip\Game\Position;
use GameHouse\BattleShip\Game\Ship;

class Easy extends AbstractAI
{
    private $orientation;

    public function setup(PlayerInterface $player)
    {
        $this->orientation = mt_rand(0, 1) ? 'H' : 'V';
        parent::setup($player);
        $random = [];
        foreach (range(Board::MIN_COLUMN, Board::MAX_COLUMN) as $col) {
            foreach (range(Board::MIN_ROW, Board::MAX_ROW) as $row) {
                $random[] = new Position($col, $row);
            }
        }
        $this->context->setRandom($random);
    }

    protected function aimToVisibleBoats()
    {
        if (mt_rand(0, 100) < 75) {
            $hits = $this->context->getHit();
            $boat = reset($hits);
            /** @var Position[] $hits */
            $hits = $boat['hits'];

            $targets = $this->getCandidates($hits);
            shuffle($targets);

            return array_shift($targets);
        } else {

            return $this->aimRandomly();
        }
    }

    /**
     * @param array $hits
     * @return Position[]
     */
    private function getCandidates(array $hits)
    {
        $surroundings = [];

        if (count($hits) === 1) {
            $hit = reset($hits);
            $cell = new Position($hit->getCol() - 1, $hit->getRow());
            $this->addIfNotDiscovered($surroundings, $cell);

            $cell = new Position($hit->getCol(), $hit->getRow() - 1);
            $this->addIfNotDiscovered($surroundings, $cell);

            $cell = new Position($hit->getCol() + 1, $hit->getRow());
            $this->addIfNotDiscovered($surroundings, $cell);

            $cell = new Position($hit->getCol(), $hit->getRow() + 1);
            $this->addIfNotDiscovered($surroundings, $cell);

        } else {
            $tmp = $hits;
            usort($tmp, [$this, 'mySort']);
            $first = array_shift($tmp);
            $last = array_pop($tmp);

            if ($first->getCol() === $last->getCol()) {
                $cell = new Position($first->getCol(), $first->getRow() - 1);
                $this->addIfNotDiscovered($surroundings, $cell);

                $cell = new Position($last->getCol(), $last->getRow() + 1);
                $this->addIfNotDiscovered($surroundings, $cell);

            } else if ($first->getRow() === $last->getRow()) {
                $cell = new Position($first->getCol() - 1, $first->getRow());
                $this->addIfNotDiscovered($surroundings, $cell);

                $cell = new Position($last->getCol() + 1, $last->getRow());
                $this->addIfNotDiscovered($surroundings, $cell);

            } else {
                $msg = '';
                foreach ($tmp as $mmm) {
                    $msg .= '[' . $mmm->asString() . ']';
                }

                return null;
            }
        }

        if (count($surroundings) === 0) {
            return null;
        }

        return $surroundings;
    }

    protected function mySort(Position $a, Position $b)
    {
        if ($a->equals($b)) return 0;
        return $a->compare($b);
    }

    protected function getPositionForShip(Ship $ship)
    {
        $result = parent::getPositionForShip($ship);
        return [$result[0], $this->orientation];
    }
}