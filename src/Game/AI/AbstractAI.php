<?php

namespace GameHouse\BattleShip\Game\AI;

use GameHouse\BattleShip\Client\LocalPlayer;
use GameHouse\BattleShip\Game\Board;
use GameHouse\BattleShip\Game\Event\ResponseShoot;
use GameHouse\BattleShip\Game\PlayerInterface;
use GameHouse\BattleShip\Game\Position;
use GameHouse\BattleShip\Game\Ship;

abstract class AbstractAI implements AIInterface
{
    /** @var PlayerInterface */
    protected $player;
    /** @var Context */
    protected $context;

    public function setup(PlayerInterface $player)
    {
        if ($this->player !== null) {
            throw new \InvalidArgumentException('AI already used for player ' . $this->player->getId());
        }

        $this->player = $player;
        $this->context = new Context($this->player);
        $this->context->initialize($this->player->getGame()->getShipLibrary());
    }

    public function handleResponseShoot(ResponseShoot $event)
    {
        $this->context->removeFromRandom($event->getPosition());

        if ($event->isHit()) {
            $this->context->shipHit($event->getShipName(), $event->getPosition());

            if ($event->isSunk()) {
                $this->context->shipSink($event->getShipName());
            }
        }
    }

    /** @return Position */
    public function getTargetPosition()
    {
        return count($this->context->getHit()) > 0 ?
            $this->aimToVisibleBoats() :
            $this->aimRandomly();
    }

    protected function aimToVisibleBoats()
    {
        return new Position(mt_rand(Board::MIN_COLUMN, Board::MAX_COLUMN), mt_rand(Board::MIN_ROW, Board::MAX_ROW));
    }

    protected function aimRandomly()
    {
        try {
            $candidates = $this->context->getRandom();

            if ($candidates === null || count($candidates) === 0) {
                throw new \LogicException('Could not get any valid position to attack');
            }

            $selected_key = array_rand($candidates);
            return $candidates[$selected_key];
        } catch(\LogicException $e) {
            return new Position(mt_rand(Board::MIN_COLUMN, Board::MAX_COLUMN), mt_rand(Board::MIN_ROW, Board::MAX_ROW));
        }
    }

    protected function addIfNotDiscovered(array &$array, Position $position)
    {
        if (!$position->isValid())
            return;

        $enemyBoard = $this->player->getGame()->getPlayers()->getOpponent($this->player)->getBoard();
        if (!$enemyBoard->getBoardCell($position)->isVisible()) {
            $array[] = $position;
        }
    }

    public function placeShips(PlayerInterface $player)
    {
        if ($player instanceof LocalPlayer) {

            foreach ($player->getGame()->getShipLibrary()->getShips() as $ship) {
                while (true) {
                    try {
                        list($cell, $orientation) = $this->getPositionForShip($ship);
                        $player->placeShip($ship->getId(), $cell, $orientation, $ship->getSize());
                        break;
                    } catch (\InvalidArgumentException $e) {
                    }
                }
            }
        }

        $hash = $player->getBoard()->getRealHash();
        $player->getBoard()->setHash($hash);
    }

    /**
     * Override on complex AI
     *
     * @param Ship $ship
     * @return array     With Position and orientation ('H'|'V')
     */
    protected function getPositionForShip(Ship $ship)
    {
        $cell = new Position(mt_rand(Board::MIN_COLUMN, Board::MAX_COLUMN), mt_rand(Board::MIN_ROW, Board::MAX_ROW));
        $orientation = mt_rand(0, 1) ? 'H' : 'V';

        return [$cell, $orientation];
    }
}