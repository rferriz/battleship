<?php

namespace GameHouse\BattleShip\Game\AI;

use GameHouse\BattleShip\Game\Board;
use GameHouse\BattleShip\Game\BoardCell;
use GameHouse\BattleShip\Game\PlayerInterface;
use GameHouse\BattleShip\Game\Position;
use GameHouse\BattleShip\Game\Ship;

class Medium extends AbstractAI
{
    protected $shipBorder = 0;

    public function setup(PlayerInterface $player)
    {
        $this->shipBorder = mt_rand(1, 2);
        parent::setup($player);
        $random = [];
        foreach (range(Board::MIN_COLUMN, Board::MAX_COLUMN) as $col) {
            foreach(range(Board::MIN_ROW, Board::MAX_ROW) as $row) {
                if (($col + $row) % 2 === 0) {
                    $random[] = new Position($col, $row);
                }
            }
        }
        $this->context->setRandom($random);
    }

    protected function aimToVisibleBoats()
    {
        $hits = $this->context->getHit();
        $boat = reset($hits);
        /** @var Position[] $hits */
        $hits = $boat['hits'];

        $targets = $this->getCandidates($hits);
        shuffle($targets);

        return array_shift($targets);
    }

    /**
     * @param array $hits
     * @return Position[]
     */
    private function getCandidates(array $hits)
    {
        $surroundings = [];

        if (count($hits) === 1) {
            $hit = reset($hits);
            $cell = new Position($hit->getCol() - 1, $hit->getRow());
            $this->addIfNotDiscovered($surroundings, $cell);

            $cell = new Position($hit->getCol(), $hit->getRow() - 1);
            $this->addIfNotDiscovered($surroundings, $cell);

            $cell = new Position($hit->getCol() + 1, $hit->getRow());
            $this->addIfNotDiscovered($surroundings, $cell);

            $cell = new Position($hit->getCol(), $hit->getRow() + 1);
            $this->addIfNotDiscovered($surroundings, $cell);

        } else {
            $tmp = $hits;
            usort($tmp, [$this, 'mySort']);
            $first = array_shift($tmp);
            $last = array_pop($tmp);

            if ($first->getCol() === $last->getCol()) {
                $cell = new Position($first->getCol(), $first->getRow() - 1);
                $this->addIfNotDiscovered($surroundings, $cell);

                $cell = new Position($last->getCol(), $last->getRow() + 1);
                $this->addIfNotDiscovered($surroundings, $cell);

            } else if ($first->getRow() === $last->getRow()) {
                $cell = new Position($first->getCol() - 1, $first->getRow());
                $this->addIfNotDiscovered($surroundings, $cell);

                $cell = new Position($last->getCol() + 1, $last->getRow());
                $this->addIfNotDiscovered($surroundings, $cell);

            } else {
                $msg = '';
                foreach($tmp as $hit) {
                    $msg .= '[' . $hit->asString() . ']';
                }
                return null;
            }
        }

        if (count($surroundings) === 0) {
            return null;
        }

        return $surroundings;
    }


    protected function mySort(Position $a, Position $b) {
        if ($a->equals($b)) return 0;
        return $a->compare($b);
    }

    protected function getPositionForShip(Ship $ship)
    {
        $result = parent::getPositionForShip($ship);

        $i = 0;
        while (true) {
            # Slowly reduce allowed ship separation (one by every 50 failed attempts)
            if ($this->canBePlaced($ship, $result[0], $result[1], $this->shipBorder - intval(round($i/50)))) {
                break;
            }
            $result = parent::getPositionForShip($ship);
            $i++;
        }

        return $result;
    }

    protected function canBePlaced(Ship $ship, Position $position, $orientation, $emptySpace = 0)
    {
        $emptySpace = max($emptySpace, 0);

        # Calculate bounding box
        $minCol = $position->getCol();
        $maxCol = $orientation === 'H' ? $position->getCol() + $ship->getSize() : $position->getCol();
        $minRow = $position->getRow();
        $maxRow = $orientation === 'V' ? $position->getRow() + $ship->getSize() : $position->getRow();

        $minCol = max(Board::MIN_COLUMN, $minCol - $emptySpace);
        $maxCol = min(Board::MAX_COLUMN, $maxCol + $emptySpace);
        $minRow = max(Board::MIN_ROW, $minRow - $emptySpace);
        $maxRow = min(Board::MAX_ROW, $maxRow + $emptySpace);

        # Check if there is anything inside
        foreach (range($minCol, $maxCol) as $col) {
            foreach (range($minRow, $maxRow) as $row) {
                $p = new Position($col, $row);
                $cell = $this->player->getBoard()->getBoardCell($p);
                if ($cell->getContent() !== BoardCell::WATER) {
                    return false;
                }
            }
        }

        return true;
    }

}