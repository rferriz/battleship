<?php

namespace GameHouse\BattleShip\Game\AI;


use GameHouse\BattleShip\Game\Event\ResponseShoot;
use GameHouse\BattleShip\Game\PlayerInterface;
use GameHouse\BattleShip\Game\Position;

interface AIInterface
{
    public function setup(PlayerInterface $player);
    public function placeShips(PlayerInterface $player);
    public function handleResponseShoot(ResponseShoot $event);
    /** @return Position */
    public function getTargetPosition();
}