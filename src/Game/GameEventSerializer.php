<?php

namespace GameHouse\BattleShip\Game;

use GameHouse\BattleShip\Game\Event\Error;
use GameHouse\BattleShip\Game\Event\GameEvent;
use GameHouse\BattleShip\Game\Event\GameResult;
use GameHouse\BattleShip\Game\Event\GameStateEvent;
use GameHouse\BattleShip\Game\Event\Identification;
use GameHouse\BattleShip\Game\Event\LeaveGame;
use GameHouse\BattleShip\Game\Event\RequestBoard;
use GameHouse\BattleShip\Game\Event\RequestBoardHash;
use GameHouse\BattleShip\Game\Event\FireShoot;
use GameHouse\BattleShip\Game\Event\ResponseBoard;
use GameHouse\BattleShip\Game\Event\ResponseBoardHash;
use GameHouse\BattleShip\Game\Event\ResponseShoot;
use GameHouse\BattleShip\Game\Event\Surrender;

class GameEventSerializer implements GameEventSerializerInterface
{
    public function deserialize(PlayerInterface $player, $input)
    {
        $playerId = $player->getId();

        $json = json_decode($input, true);
        if ($json === null) {
            throw new \InvalidArgumentException(sprintf('Invalid json string: ' . $input));
        }

        if (!array_key_exists('type', $json)) {
            throw new \InvalidArgumentException('Json object does not have required field "type"');
        }

        if ($json['type'] === 'identification')
            return new Identification($playerId, $json['player_name']);

        if ($json['type'] === 'error')
            return new Error($playerId, $json['message']);

        if ($json['type'] === 'request_board_hash')
            return new RequestBoardHash($playerId);

        if ($json['type'] === 'board_hash')
            return new ResponseBoardHash($playerId, $json['hash']);

        if ($json['type'] === 'request_board')
            return new RequestBoard($playerId);

        if ($json['type'] === 'response_board')
            return new ResponseBoard($playerId, $json['board'], $json['legend']);

        if ($json['type'] === 'fire')
            return new FireShoot($playerId, Position::createFromString($json['position']));

        if ($json['type'] === 'damage_report')
            return $this->createResponseShoot($playerId, $json);

        if ($json['type'] === 'surrender')
            return new Surrender($playerId);

        if ($json['type'] === 'leave_game')
            return new LeaveGame($playerId);

        if ($json['type'] === 'game_state') {
            $active = $player->getGame()->getPlayers()->getByName($json['active_player']);
            $passive = $player->getGame()->getPlayers()->getByName($json['passive_player']);

            return new GameStateEvent($playerId, $active, $passive, $json['state']);
        }

        if ($json['type'] === 'game_result') {
            return new GameResult($playerId, $json['winner'], $json['loser'], $json['message']);
        }

        throw new \InvalidArgumentException(sprintf('Could not handle json object of type %s  [%s]', $json['type'], $input));
    }

    private function createResponseShoot($playerId, $json)
    {
        if (!array_key_exists('position', $json) || !array_key_exists('result', $json)) {
            throw new \InvalidArgumentException(sprintf('Json object type %s requires field "position" and "result"', $json['type']));
        }

        $position = Position::createFromString($json['position']);
        @list($hitOrMiss, $shipName, $sink) = explode(':', $json['result']);

        if ($hitOrMiss !== 'hit' && $hitOrMiss !== 'miss') {
            throw new \InvalidArgumentException(sprintf('Expected hit or miss, bur received: %s', $hitOrMiss));
        }

        $isSink = ($sink === 'sink' || $sink === 'sunk');

        return new ResponseShoot($playerId, $position, $hitOrMiss, $shipName, $isSink);
    }

    public function serialize(GameEvent $event)
    {
        if ($event instanceof Identification) {
            return json_encode([
                'type' => 'identification',
                'player_name' => $event->getName(),
            ]);
        }

        if ($event instanceof Error) {
            return json_encode([
                'type' => 'error',
                'message' => $event->getMessage(),
            ]);
        }

        if ($event instanceof LeaveGame) {
            return json_encode([
                'type' => 'leave_game',
            ]);
        }

        if ($event instanceof RequestBoardHash) {
            return json_encode([
                'type' => 'request_board_hash',
            ]);
        }

        if ($event instanceof ResponseBoardHash) {
            return json_encode([
                'type' => 'board_hash',
                'hash' => $event->getHash(),
            ]);
        }

        if ($event instanceof GameStateEvent) {
            return json_encode([
                'type' => 'game_state',
                'state' => $event->getState(),
                'active_player' => $event->getActivePlayerName(),
                'passive_player' => $event->getPassivePlayerName(),
            ]);
        }

        if ($event instanceof FireShoot) {
            return json_encode([
                'type' => 'fire',
                'position' => $event->getPosition()->asString(),
            ]);
        }

        if ($event instanceof ResponseShoot) {
            $result = $event->isMiss() ? 'miss' : sprintf('hit:%s', $event->getShipName());
            if ($event->isSunk()) $result .= ':sunk';

            return json_encode([
                'type' => 'damage_report',
                'position' => $event->getPosition()->asString(),
                'result' => $result,
            ]);
        }

        if ($event instanceof Surrender) {
            return json_encode([
                'type' => 'surrender',
                'reason' => ''
            ]);
        }

        if ($event instanceof RequestBoard) {
            return json_encode([
                'type' => 'request_board'
            ]);
        }

        if ($event instanceof ResponseBoard) {
            return json_encode([
                'type' => 'response_board',
                'board' => $event->getBoard(),
                'legend' => $event->getLegend(),
            ]);
        }

        if ($event instanceof GameResult) {
            return json_encode([
                'type' => 'game_result',
                'winner' => $event->getWinner(),
                'loser' => $event->getLoser(),
                'message' => $event->getMessage(),
            ]);
        }
        throw new \InvalidArgumentException(sprintf('Do not know how to deserialize %s', get_class($event)));
    }
}