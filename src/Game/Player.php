<?php

namespace GameHouse\BattleShip\Game;

use GameHouse\BattleShip\Game\Event\GameEvent;

class Player implements PlayerInterface
{
    private $id;
    private $name;
    private $board;
    /** @var Game */
    private $game;
    private $library;

    public function __construct()
    {
        $this->id = mt_rand(1000, 9999) . mt_rand(1000, 9999);
        $this->board = new Board();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getBoard()
    {
        return $this->board;
    }

    public function setGame(Game $game)
    {
        $this->game = $game;
    }

    /** @return Game */
    public function getGame()
    {
        return $this->game;
    }

    public function getLibrary()
    {
        if (is_null($this->library)) {
            return $this->game->getShipLibrary();
        }

        return $this->library;
    }

    public function setLibrary(ShipsLibrary $library)
    {
        // Only allowed to be set once
        if ($this->library === null) {
            $this->library = $library;
        }
    }

    public function notify(GameEvent $event)
    {
        throw new \LogicException('Method notify() not implemented.');
    }

    public function __toString()
    {
        return sprintf('[%s]', $this->name);
    }
}