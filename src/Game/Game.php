<?php

namespace GameHouse\BattleShip\Game;

use GameHouse\BattleShip\Game\Event\Error;
use GameHouse\BattleShip\Game\Event\GameEvent;
use GameHouse\BattleShip\Game\Event\GameResult;
use GameHouse\BattleShip\Game\Event\GameStateEvent;
use GameHouse\BattleShip\Game\Event\Identification;
use GameHouse\BattleShip\Game\Event\LeaveGame;
use GameHouse\BattleShip\Game\Event\RequestBoard;
use GameHouse\BattleShip\Game\Event\RequestBoardHash;
use GameHouse\BattleShip\Game\Event\FireShoot;
use GameHouse\BattleShip\Game\Event\ResponseBoard;
use GameHouse\BattleShip\Game\Event\ResponseBoardHash;
use GameHouse\BattleShip\Game\Event\ResponseShoot;
use GameHouse\BattleShip\Game\Event\Surrender;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use React\Promise\Deferred;

class Game
{
    use LoggerAwareTrait;

    private $state;

    /** @var Deferred */
    private $deferred;

    /** @var GameEventValidator */
    private $eventValidator;

    /** @var Players */
    private $players;

    /** @var Position */
    private $lastShoot;

    /** @var ShipsLibrary */
    private $library;

    /** @var string[] */
    private $cheaters;

    /** @var string  */
    private $winner;

    /** @var boolean */
    private $server;

    public function __construct(Players $players, ShipsLibrary $library, GameEventValidator $eventValidator, LoggerInterface $logger)
    {
        $this->eventValidator = $eventValidator;
        $this->players = $players;
        $this->state = GameState::INVALID;
        $this->lastShoot = null;
        $this->cheaters = [];
        $this->library = $library;
        $this->server = false;
        $this->logger = $logger !== null ? $logger : new NullLogger();
    }

    public function isServer($master = null) {
        if ($master !== null) {
            $this->server = ($master === true);
        }

        return $this->server === true;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getShipLibrary() {
        return $this->library;
    }

    public function start(Deferred $deferred)
    {
        if ($this->state !== GameState::INVALID)
            throw new \InvalidArgumentException('This game has been already started');

        $this->deferred = $deferred;
        $this->state = GameState::SETUP;
        foreach ($this->players->getPlayers() as $player) {
            $player->setGame($this);
            $player->notify(new RequestBoardHash($player->getId()));
        }
    }

    public function getPlayers()
    {
        return $this->players;
    }

    public function receiveEvent(PlayerInterface $player, GameEvent $event)
    {
        try {

            if (!$this->eventValidator->isAllowed($this, $player, $event)) {
                throw new \InvalidArgumentException(sprintf('GameEvent %s not allowed on current game phase (%s)', get_class($event), $this->state));
            }

            $this->processGameEvent($player, $event);

            $this->updateGameState($event);
        } catch (\InvalidArgumentException $e) {
            $this->logger->error($e->getMessage() . PHP_EOL . $e->getTraceAsString());
        }
    }

    public function isPlaying()
    {
        return
            ($this->state === GameState::ACTIVE_SHOULD_FIRE) ||
            ($this->state === GameState::PASSIVE_SHOULD_REPORT);
    }

    public function isAntiCheat()
    {
        return
            ($this->state === GameState::ANTI_CHEAT);
    }

    protected function updateGameState(GameEvent $event)
    {
        $currentState = $this->state;
        $active = $this->players->getActive();
        $passive = $this->players->getPassive();

        if ($currentState === GameState::SETUP) {

            $number_of_hashes = 0;
            $number_of_names = 0;
            foreach ($this->players->getPlayers() as $current) {
                if ($current->getName() !== null) {
                    $number_of_names++;
                }
                if ($current->getBoard()->getHash() !== null) {
                    $number_of_hashes++;
                }
            }

            if ($number_of_hashes === 2 && $number_of_names === 2) {
                $this->state = GameState::ACTIVE_SHOULD_FIRE;

                if ($this->isServer()) {
                    $newEvent = new GameStateEvent($active->getId(), $active, $passive, GameState::ACTIVE_SHOULD_FIRE);
                    $active->notify($newEvent);
                    $passive->notify($newEvent);
                }
            }
        }

        if ($currentState === GameState::ACTIVE_SHOULD_FIRE) {
            if ($event instanceof FireShoot) {
                $this->state = GameState::PASSIVE_SHOULD_REPORT;

                if ($this->isServer()) {
                    $newEvent = new GameStateEvent($active->getId(), $active, $passive, GameState::PASSIVE_SHOULD_REPORT);
                    $active->notify($newEvent);
                    $passive->notify($newEvent);
                }
            }
        }

        if ($currentState === GameState::PASSIVE_SHOULD_REPORT) {
            if ($event instanceof ResponseShoot) {
                $this->state = GameState::ACTIVE_SHOULD_FIRE;
                $this->players->next();
                if ($this->isServer()) {
                    $newEvent = new GameStateEvent($active->getId(), $passive, $active, GameState::ACTIVE_SHOULD_FIRE);
                    $active->notify($newEvent);
                    $passive->notify($newEvent);
                }
            }
        }

        if ($event instanceof Surrender) {
            $this->state = GameState::ANTI_CHEAT;
        }

        if ($currentState === GameState::ANTI_CHEAT) {
            $number_of_valid_hashes = 0;

            foreach ($this->players->getPlayers() as $current) {
                if ($current->getBoard()->getHash() === $current->getBoard()->getRealHash()) {
                    $number_of_valid_hashes++;
                }
            }

            $this->logger->debug('Number of valid hashes: ' . $number_of_valid_hashes);
            $this->logger->debug('Number of cheaters: ' . count($this->cheaters));

            if (($number_of_valid_hashes + count($this->cheaters)) >= 2) {
                $this->state = GameState::FINISHED;

                # Cases
                # Both cheaters, both lose
                if (count($this->cheaters) > 1) {
                    $this->logger->info('Both cheated, curse them');

                } else if (in_array($this->winner, $this->cheaters)) {

                    $this->logger->info('Winner has cheated, concede game to opponent');
                    $realWinner = null;
                    foreach($this->players->getPlayers() as $player) {
                        if ($player->getName() === $this->winner) {

                        } else {
                            $realWinner = $player->getName();
                        }
                    }
                    $this->winner = $realWinner;
                } else {
                    $this->logger->info('Winner really played fair');
                }

                $resolve = [];
                foreach($this->players->getPlayers() as $player) {

                    if ($player->getName() === $this->winner) {
                        if ($this->isServer()) {
                            $player->notify(new GameResult($player->getId(), $this->winner, [$this->players->getOpponent($player)->getName()], 'You win'));
                        }

                        $resolve[] = [
                            'player' => $player->getName(),
                            'result' => 'win',
                        ];
                    } else if (in_array($player, $this->cheaters)) {
                        if ($this->isServer()) {
                            $player->notify(new GameResult($player->getId(), $this->winner, $this->cheaters, 'You cheated'));
                        }

                        $resolve[] = [
                            'player' => $player->getName(),
                            'result' => 'lose',
                        ];
                    } else {
                        if ($this->isServer()) {
                            $player->notify(new GameResult($player->getId(), $this->winner, [$player->getName()], 'You lose'));
                        }

                        $resolve[] = [
                            'player' => $player->getName(),
                            'result' => 'lose',
                        ];
                    }
                }

                $this->deferred->resolve($resolve);
            }
            $this->logger->debug('Valid hashes: ' . $number_of_valid_hashes . ', number of cheaters: ' . count($this->cheaters));
        }

        $this->logger->debug('Current state: ' . $this->state);
    }

    protected function processGameEvent(PlayerInterface $player, GameEvent $event)
    {
        if ($player->getId() !== $event->getPlayerId()) {
            throw new \InvalidArgumentException('GameEvent is not for this player');
        }

        $this->logger->info((string)$player . ' ' . get_class($event));

        if ($event instanceof Error) {
            $this->logger->error($player->getId() . ' error: ' . $event->getMessage());
            $this->players->getOpponent($player)->notify($event);

        } else if ($event instanceof Identification) {
            $this->logger->debug($player->getId() . ' name: ' . $event->getName());
            $player->setName($event->getName());
            $this->players->getOpponent($player)->notify($event);

        } else if ($event instanceof LeaveGame) {
            $enemy = $this->players->getOpponent($player);
            $player->notify($event);
            $enemy->notify($event);

            $this->deferred->resolve([
                [
                    'player' => $player->getName(),
                    'result' => 'Abandon',
                ], [
                    'player' => $enemy->getName(),
                    'result' => 'Win'
                ]
            ]);

        } else if ($event instanceof FireShoot) {
            $this->handlePostShoot($player, $event);

        } else if ($event instanceof ResponseShoot) {
            $this->handleResponseShoot($player, $event);

        } else if ($event instanceof RequestBoard) {
            $this->handleRequestBoard($player, $event);

        } else if ($event instanceof ResponseBoard) {
            $this->handleResponseBoard($player, $event);

        } else if ($event instanceof RequestBoardHash) {
            $this->handleRequestBoardHash($player, $event);

        } else if ($event instanceof ResponseBoardHash) {
            $player->getBoard()->setHash($event->getHash());
            $this->logger->info((string)$player . ' board has: ' . $event->getHash());
            $this->players->getOpponent($player)->notify($event);

        } else if ($event instanceof GameStateEvent) {
            $this->handleGameStateEvent($player, $event);

        } else if ($event instanceof GameResult) {
            $this->logger->info('GameResult received, proxy to the receiver');
            $this->players->getOpponent($player)->notify($event);

        } else if ($event instanceof Surrender) {
            $this->logger->info((string)$player . ' surrenders');
            $opponent = $this->players->getOpponent($player);
            $this->winner = $opponent->getName();
            $opponent->notify($event);
            $this->state = GameState::ANTI_CHEAT;

            foreach($this->players->getPlayers() as $player) {
                $player->notify(new RequestBoard($player->getId()));
            }
        } else {
            $this->logger->error((string)$player . ' do not know how to process ' . get_class($event));

        }
    }

    private function handlePostShoot(PlayerInterface $player, FireShoot $event)
    {
        if ($this->lastShoot !== null) {
            throw new \InvalidArgumentException('Last Shoot has value!!!');
        }

        $enemy = $this->players->getOpponent($player);
        $enemyBoard = $enemy->getBoard();
        if ($enemyBoard->getBoardCell($event->getPosition())->isVisible()) {
            throw new \InvalidArgumentException('Position already fired');
        }

        $this->lastShoot = $event->getPosition();

        $enemy->notify($event);
    }

    private function handleResponseShoot(PlayerInterface $player, ResponseShoot $event)
    {
        if ($this->lastShoot === null) {
            throw new \InvalidArgumentException('Previous Shoot still not resolved!');
        }

        if ($this->lastShoot->asString() !== $event->getPosition()->asString()) {
            throw new \InvalidArgumentException(sprintf('Response shoot is not expected ! %s != %s',
                $event->getPosition()->asString(), $this->lastShoot->asString()));
        }

        $boardCell = $player->getBoard()->getBoardCell($event->getPosition());
        $boardCell->setVisible();

        if ($event->isHit()) {
            $ship = $this->library->findByName($event->getShipName());
            if ($boardCell->getContent() !== $ship->getId()) {
                $boardCell->setContent($ship->getId());
            }
        }

        $this->lastShoot = null;
        $this->players->getOpponent($player)->notify($event);
    }

    private function handleResponseBoard(PlayerInterface $player, ResponseBoard $event)
    {
        try {
            $realBoard = Board::createFromString($event->getBoard());
            $realBoard->setHash($realBoard->getRealHash());
        } catch(\InvalidArgumentException $e) {
            $player->notify(new Error($player->getId(), $e->getMessage()));
            return;
        }

        $realShips = new ShipsLibrary($event->getLegend());
        $player->setLibrary($realShips);

        // TODO Check libraries, must have same number of ships, name and size.

        try {
            if ($player->getBoard()->getHash() !== $realBoard->getHash()) {
                throw new \InvalidArgumentException(sprintf('Hash does not match with previously supplied.' . PHP_EOL . $player->getBoard()->getHash() . PHP_EOL . $realBoard->getRealHash()));
            }

            // Check all discovered cells on player board matches with real board
            if (!Board::checkEquivalent($player->getBoard(), $player->getGame()->getShipLibrary(), $realBoard, $realShips)) {
                $this->logger->error($player->getId() . ' IS A CHEATER');
                if (!in_array($player->getName(), $this->cheaters)) {
                    $this->cheaters[] = $player->getName();
                }
                throw new \InvalidArgumentException('Not equivalent board detected, CHEATER!!!!');
            }
        } catch(\InvalidArgumentException $e) {
            $player->notify(new Error($player->getId(), $e->getMessage()));
        }

        // Copy board to make it available inside game to the other player
        foreach(range(Board::MIN_COLUMN, Board::MAX_COLUMN) as $col) {
            foreach(range(Board::MIN_ROW, Board::MAX_ROW) as $row) {
                try {
                    $position = new Position($col, $row);
                    $boardCell = $player->getBoard()->getBoardCell($position);
                    $otherCell = $realBoard->getBoardCell($position);
                    if ($otherCell->getContent() !== BoardCell::WATER) {
                        $realShip = $realShips->findById($otherCell->getContent());
                        $boardCell->setContent($realShip->getId(), true);
                    }
                } catch (\InvalidArgumentException $e) {
                    echo $e->getMessage() . PHP_EOL . $e->getTraceAsString() . PHP_EOL . PHP_EOL;
                }
            }
        }
        $this->players->getOpponent($player)->notify($event);
    }

    private function handleRequestBoardHash(PlayerInterface $player, RequestBoardHash $event)
    {
        $this->logger->info((string)$player . ' asked for hash');
        $enemy = $this->players->getOpponent($player);
        if ($enemy->getBoard()->getHash() !== null) {
            $this->logger->info((string)$player . ' I HAVE the hash');
            $player->notify(new ResponseBoardHash($enemy->getId(), $enemy->getBoard()->getHash()));
        } else {
            $this->logger->info((string)$player . ' querying to ' . (string)$enemy . '  to get the hash');
            $enemy->notify($event);
        }
    }

    /**
     * @param PlayerInterface $player
     * @param GameStateEvent $event
     */
    protected function handleGameStateEvent(PlayerInterface $player, GameStateEvent $event)
    {
        /* Fix active / inactive player */
        $active = $this->players->getActive();
        $passive = $this->players->getPassive();

        if ($active->getName() !== $event->getActivePlayerName() && $passive->getName() === $event->getActivePlayerName()) {
            $this->players->next();
            $this->logger->warning('Switched active player!');
        }

        $active = $this->players->getActive();
        if ($active->getName() !== $event->getActivePlayerName()) {
            $this->logger->error(sprintf('Could not make player %s active player!', $event->getActivePlayerName()));
        }

        if ($this->state !== $event->getState()) {
            $old = $this->state;
            $this->state = $event->getState();
            $this->logger->warning('Switched game phase from ' . $old . ' to ' . $this->state);
        }

        $this->players->getOpponent($player)->notify($event);
    }

    protected function handleRequestBoard(PlayerInterface $player, RequestBoard $event)
    {
        $opponent = $this->getPlayers()->getOpponent($player);

        // If I have the real board, just serve it!
        if ($opponent->getBoard()->getHash() === $opponent->getBoard()->getRealHash()) {
            $player->notify(new ResponseBoard($opponent->getId(), $opponent->getBoard()->asString(), $opponent->getLibrary()->asLegend()));
            return;
        } else {
            // Just act as proxy
            $opponent->notify($event);
        }
    }
}