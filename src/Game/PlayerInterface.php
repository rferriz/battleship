<?php

namespace GameHouse\BattleShip\Game;


use GameHouse\BattleShip\Game\Event\GameEvent;

interface PlayerInterface
{
    /**
     * Player internal id
     *
     * @return string
     */
    public function getId();

    /**
     * Player name (expected to be unique)
     *
     * @return string
     */
    public function getName();

    public function setName($name);

    /**
     * @return Board
     */
    public function getBoard();

    /**
     * @param Game $game
     */
    public function setGame(Game $game);

    /** @return Game */
    public function getGame();

    /** @return ShipsLibrary */
    public function getLibrary();

    public function setLibrary(ShipsLibrary $library);

    /**
     * @param GameEvent $event
     */
    public function notify(GameEvent $event);
}