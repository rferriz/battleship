<?php

namespace GameHouse\BattleShip\Game;

/**
 * Position
 *
 * @author Raul Ferriz <raul.ferriz@gmail.com>
 */
class Position
{
    const ROW_LETTERS = true;
    const MIN_COL = 1;
    const MAX_COL = 10;
    const MIN_ROW = 1;
    const MAX_ROW = 10;

    private $col;
    private $row;

    public function __construct($col, $row)
    {
        $this->col = $col;
        $this->row = $row;
    }

    public function getCol()
    {
        return $this->col;
    }

    public function getRow()
    {
        return $this->row;
    }

    public function isValid()
    {
        if ($this->getCol() < static::MIN_COL || $this->getCol() > static::MAX_COL ||
            $this->getRow() < static::MIN_ROW || $this->getRow() > static::MAX_ROW) {
            return false;
        }

        return true;
    }

    public static function createFromString($string)
    {
        if (strpos($string, ':') === false) {
            throw new \InvalidArgumentException('Not a valid position');
        }
        list($row, $col) = explode(':', $string);
        if (static::ROW_LETTERS) {
            $row = static::letterToNumber($row);
        } else {
            $col = static::letterToNumber($col);
        }

        if (
            $col < static::MIN_COL
            || $col > static::MAX_COL
            ||$row < static::MIN_ROW
            || $row > static::MAX_ROW
        ) {
            throw new \InvalidArgumentException('Not a valid position');
        }

        return new static($col, $row);
    }


    public function asString()
    {
        return $this->getRowAsString() . ':' . $this->getColAsString();
    }

    public function equals(Position $other)
    {
        return
            ($this->col === $other->col) &&
            ($this->row === $other->row);
    }

    public function compare(Position $other)
    {
        if ($this->equals($other)) return 0;
        $hash = $this->col + $this->row * 256;
        $otherHash = $other->col + $other->row * 256;

        return ($hash < $otherHash) ? -1 : 1;
    }

    public function getColAsString()
    {
        if (!static::ROW_LETTERS) {
            return static::numberToLetter($this->col);
        }

        return $this->col;
    }

    public function getRowAsString()
    {
        if (static::ROW_LETTERS) {
            return static::numberToLetter($this->row);
        }

        return $this->row;
    }

    public static function numberToLetter($number) {
        return chr(ord('A') + $number - 1);
    }

    public static function letterToNumber($letter) {
        return ord(mb_strtoupper($letter)) - ord('A') + 1;
    }

}