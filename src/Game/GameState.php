<?php

namespace GameHouse\BattleShip\Game;


class GameState
{
    const INVALID = 'not.started';
    const SETUP = 'game.setup';
    const ACTIVE_SHOULD_FIRE = 'wait.for.active.shoot';
    const PASSIVE_SHOULD_REPORT = 'wait.for.passive.report';
    const ANTI_CHEAT = 'anti-cheat';
    const FINISHED = 'finished';
}