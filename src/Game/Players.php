<?php

namespace GameHouse\BattleShip\Game;

/**
 * Class Players
 * Helper class to handle player turn
 *
 * @author Raul Ferriz <raul.ferriz@gmail.com>
 */
class Players
{
    private $active;
    private $passive;

    public function __construct(PlayerInterface $active, PlayerInterface $passive)
    {
        $this->active = $active;
        $this->passive = $passive;
    }

    public function next()
    {
        $active = $this->active;
        $passive = $this->passive;

        $this->active = $passive;
        $this->passive = $active;

        return $this->active;
    }

    public function getOpponent(PlayerInterface $player)
    {
        if ($this->isActive($player))
            return $this->passive;

        if ($this->isPassive($player))
            return $this->active;

        return null;
    }

    public function isActive(PlayerInterface $active)
    {
        return $this->active->getId() === $active->getId();
    }

    public function isPassive(PlayerInterface $passive)
    {
        return $this->passive->getId() === $passive->getId();
    }

    public function getActive()
    {
        return $this->active;
    }

    public function getPassive()
    {
        return $this->passive;
    }

    /**
     * @return PlayerInterface[]
     */
    public function getPlayers()
    {
        return [
            $this->active,
            $this->passive,
        ];
    }

    public function getByName($name)
    {
        foreach ($this->getPlayers() as $player) {
            if ($player->getName() === $name) {
                return $player;
            }
        }

        throw new \InvalidArgumentException(sprintf('Player with name %s not found.', $name));

    }
}