<?php

namespace GameHouse\BattleShip\Game\Event;

class LeaveGame implements GameEvent
{
    use PlayerIdAwareTrait;

    public function __construct($playerId, $message = '')
    {
        $this->playerId = $playerId;
        if (mb_strlen($message) > 1024) {
            throw new \InvalidArgumentException(sprintf('Maximum expected message length: 1024. Current: %d', mb_strlen($message)));
        }
    }
}