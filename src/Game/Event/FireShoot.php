<?php

namespace GameHouse\BattleShip\Game\Event;

use GameHouse\BattleShip\Game\Position;

class FireShoot implements GameEvent
{
    use PlayerIdAwareTrait;

    /**
     * @var Position
     */
    private $position;

    public function __construct($playerId, Position $position)
    {
        $this->playerId = $playerId;
        $this->position = $position;
    }

    public function getPosition()
    {
        return $this->position;
    }

}