<?php

namespace GameHouse\BattleShip\Game\Event;

class Identification implements GameEvent
{
    use PlayerIdAwareTrait;

    private $name;

    public function __construct($playerId, $name)
    {
        $this->playerId = $playerId;
        $this->name = $name;

        if (mb_strlen($this->name) < 1) {
            throw new \InvalidArgumentException('Empty player name');
        }

        if (mb_strlen($this->name) > 64) {
            throw new \InvalidArgumentException(sprintf('Expected a name length lesser than 64 bytes, %d received', mb_strlen($name)));
        }
    }

    public function getName()
    {
        return $this->name;
    }
}