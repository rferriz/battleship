<?php

namespace GameHouse\BattleShip\Game\Event;

trait PlayerIdAwareTrait
{
    private $playerId;

    public function getPlayerId()
    {
        return $this->playerId;
    }

}