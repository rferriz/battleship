<?php

namespace GameHouse\BattleShip\Game\Event;

use GameHouse\BattleShip\Game\Position;

class ResponseShoot implements GameEvent
{
    use PlayerIdAwareTrait;

    /**
     * @var Position
     */
    private $position;

    private $hitOrMiss;
    private $shipName;
    private $sunk;

    public function __construct($playerId, Position $position, $hitOrMiss, $shipName = '', $sunk = false)
    {
        $this->playerId = $playerId;
        $this->position = $position;
        $this->hitOrMiss = $hitOrMiss;
        $this->shipName = $shipName;
        $this->sunk = $sunk === true;

        if ($hitOrMiss !== 'hit' && $hitOrMiss !== 'miss') {
            throw new \InvalidArgumentException('hitOrMiss is ' . $hitOrMiss);
        }
    }

    /**
     * @return Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return boolean
     */
    public function isHit()
    {
        return $this->hitOrMiss === 'hit';
    }

    /**
     * @return boolean
     */
    public function isMiss()
    {
        return $this->hitOrMiss === 'miss';
    }

    /**
     * @return string
     */
    public function getShipName()
    {
        return $this->shipName;
    }

    /**
     * @return boolean
     */
    public function isSunk()
    {
        return $this->sunk;
    }

}