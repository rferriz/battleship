<?php

namespace GameHouse\BattleShip\Game\Event;


class GameResult implements GameEvent
{
    use PlayerIdAwareTrait;

    /** @var string */
    private $message;

    /** @var string */
    private $winner;

    /** @var array */
    private $loser;

    /**
     * GameResult constructor.
     * @param $playerId
     * @param $winner
     * @param array $loser      There may be more than one loser if both players cheat!
     * @param string $message   Displayed to player
     */
    public function __construct($playerId, $winner, array $loser, $message)
    {
        $this->playerId = $playerId;
        $this->winner = $winner;
        $this->loser = $loser;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * @return array
     */
    public function getLoser()
    {
        return $this->loser;
    }
}