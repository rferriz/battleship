<?php

namespace GameHouse\BattleShip\Game\Event;

class Error implements GameEvent
{
    use PlayerIdAwareTrait;

    /**
     * @var string
     */
    private $message;

    public function __construct($playerId, $message)
    {
        $this->playerId = $playerId;
        if (mb_strlen($message) > 1024) {
            throw new \InvalidArgumentException(sprintf('Maximum expected message length: 1024. Current: %d', mb_strlen($message)));
        }
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }

}