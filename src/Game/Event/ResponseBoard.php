<?php

namespace GameHouse\BattleShip\Game\Event;

class ResponseBoard implements GameEvent
{
    use PlayerIdAwareTrait;

    private $board;
    private $legend;

    public function __construct($playerId, $board, $legend)
    {
        $this->playerId = $playerId;
        $this->board = $board;
        $this->legend = $legend;
    }

    public function getBoard()
    {
        return $this->board;
    }

    public function getLegend()
    {
        return $this->legend;
    }
}