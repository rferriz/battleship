<?php

namespace GameHouse\BattleShip\Game\Event;

use GameHouse\BattleShip\Game\GameState;
use GameHouse\BattleShip\Game\PlayerInterface;

class GameStateEvent implements GameEvent
{
    use PlayerIdAwareTrait;

    /**
     * @var string
     */
    private $state;

    private $active;
    private $passive;

    public function __construct($playerId,  PlayerInterface $active, PlayerInterface $passive, $state)
    {
        if (!in_array($state, static::getValidStates())) {
            throw new \InvalidArgumentException(sprintf('Invalid game state %s', $state));
        }
        $this->playerId = $playerId;
        $this->state = $state;
        $this->active = $active->getName();
        $this->passive = $passive->getName();
    }

    public function getState()
    {
        return $this->state;
    }

    public function getActivePlayerName()
    {
        return $this->active;
    }

    public function getPassivePlayerName()
    {
        return $this->passive;
    }

    private function getValidStates()
    {
        return [
            GameState::INVALID => GameState::INVALID,
            GameState::SETUP => GameState::SETUP,
            GameState::ACTIVE_SHOULD_FIRE => GameState::ACTIVE_SHOULD_FIRE,
            GameState::PASSIVE_SHOULD_REPORT => GameState::PASSIVE_SHOULD_REPORT,
            GameState::ANTI_CHEAT => GameState::ANTI_CHEAT,
            GameState::FINISHED => GameState::FINISHED,
        ];
    }
}