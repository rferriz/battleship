<?php

namespace GameHouse\BattleShip\Game\Event;


interface GameEvent
{
    public function getPlayerId();
}