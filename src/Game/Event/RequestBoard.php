<?php

namespace GameHouse\BattleShip\Game\Event;

class RequestBoard implements GameEvent
{
    use PlayerIdAwareTrait;

    public function __construct($playerId)
    {
        $this->playerId = $playerId;
    }
}