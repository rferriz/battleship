<?php

namespace GameHouse\BattleShip\Game\Event;


class ResponseBoardHash implements GameEvent
{
    use PlayerIdAwareTrait;

    private $hash;

    public function __construct($playerId, $hash)
    {
        $this->playerId = $playerId;
        $this->hash = $hash;
    }

    public function getHash()
    {
        return $this->hash;
    }

}