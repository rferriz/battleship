<?php

namespace GameHouse\BattleShip\Game;


class ShipsLibrary
{
    /**
     * @var null|Ship[]
     */
    private $ships = null;

    public function __construct($legend = null)
    {
        if ($legend === null) {
            $alphabet = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'));
            shuffle($alphabet);
            $legend = sprintf('%s:carrier:5:C,%s:battleship:4:b,%s:cruiser:3:c,%s:submarine:3:s,%s:destroyer:2:d',
                array_shift($alphabet),
                array_shift($alphabet),
                array_shift($alphabet),
                array_shift($alphabet),
                array_shift($alphabet));
        }

        $ships = explode(',', $legend);
        foreach ($ships as $chunk) {
            $chunks = explode(':', $chunk);
            $id = $chunks[0];
            $name = $chunks[1];
            $size = $chunks[2];
            $publicId = count($chunks) > 3 ? $chunks[3] : $id;

            $this->ships[] = new Ship($id, $publicId, $size, $name);
        }
    }

    public function getShips()
    {
        return $this->ships;
    }

    /**
     * @param $id
     * @return Ship
     */
    public function findById($id)
    {
        foreach($this->ships as $ship) {
            if ($ship->getId() === $id) {
                return $ship;
            }
        }

        throw new \InvalidArgumentException('Invalid ship id: ' . $id);
    }

    /**
     * @param $name
     * @return Ship
     */
    public function findByName($name)
    {
        foreach($this->ships as $ship) {
            if ($ship->getName() === mb_strtolower($name)) {
                return $ship;
            }
        }

        throw new \InvalidArgumentException('Invalid ship name: ' . $name);
    }

    public function asLegend()
    {
        $output = '';

        foreach($this->ships as $ship) {
            $output .= sprintf('%s:%s:%d,', $ship->getId(), $ship->getName(), $ship->getSize());
        }
        $output = substr($output, 0, -1);

        return $output;
    }
}