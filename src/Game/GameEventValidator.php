<?php

namespace GameHouse\BattleShip\Game;

use GameHouse\BattleShip\Game\Event\Error;
use GameHouse\BattleShip\Game\Event\GameEvent;
use GameHouse\BattleShip\Game\Event\GameResult;
use GameHouse\BattleShip\Game\Event\GameStateEvent;
use GameHouse\BattleShip\Game\Event\Identification;
use GameHouse\BattleShip\Game\Event\LeaveGame;
use GameHouse\BattleShip\Game\Event\RequestBoard;
use GameHouse\BattleShip\Game\Event\RequestBoardHash;
use GameHouse\BattleShip\Game\Event\FireShoot;
use GameHouse\BattleShip\Game\Event\ResponseBoard;
use GameHouse\BattleShip\Game\Event\ResponseBoardHash;
use GameHouse\BattleShip\Game\Event\ResponseShoot;
use GameHouse\BattleShip\Game\Event\Surrender;

class GameEventValidator
{
    private $players;

    public function __construct(Players $players)
    {
        $this->players = $players;
    }

    /**
     * Check if $event is allowed on current game
     *
     * @param Game $game
     * @param PlayerInterface $player
     * @param GameEvent $event
     * @return bool
     */
    public function isAllowed(Game $game, PlayerInterface $player, GameEvent $event)
    {
        if ($event instanceof Error) {
            return true;
        }

        if ($event instanceof LeaveGame) {
            return true;
        }

        if ($event instanceof GameStateEvent) {
            return $game->isServer() === false;
        }

        switch ($game->getState()) {
            case GameState::SETUP:
                return $this->isAllowedOnSetup($event);
            case GameState::ANTI_CHEAT:
                return $this->isAllowedOnAntiCheat($event);
            case GameState::ACTIVE_SHOULD_FIRE:
                return $this->isAllowedOnActive($player, $event);
            case GameState::PASSIVE_SHOULD_REPORT:
                return $this->isAllowedOnPassive($player, $event);
            case GameState::FINISHED:
                return $event instanceof GameResult;
                //return false;
        }

        return false;
    }

    private function isAllowedOnSetup(GameEvent $event)
    {
        return
            ($event instanceof Identification) ||
            ($event instanceof RequestBoardHash) ||
            ($event instanceof ResponseBoardHash);
    }

    private function isAllowedOnAntiCheat(GameEvent $event)
    {
        return
            ($event instanceof RequestBoard) ||
            ($event instanceof ResponseBoard);
    }

    private function isAllowedOnActive(PlayerInterface $player, GameEvent $event)
    {
        if ($this->isAllowedOnPlaying($event)) {
            return true;
        }

        if ($this->players->isActive($player))
            return ($event instanceof FireShoot);

        return false;
    }

    private function isAllowedOnPassive(PlayerInterface $player, GameEvent $event)
    {
        if ($this->isAllowedOnPlaying($event)) {
            return true;
        }

        if ($this->players->isPassive($player))
            return ($event instanceof ResponseShoot);

        return false;
    }

    private function isAllowedOnPlaying(GameEvent $event)
    {
        return ($event instanceof Surrender);
    }
}