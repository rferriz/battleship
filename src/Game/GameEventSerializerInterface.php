<?php

namespace GameHouse\BattleShip\Game;

use GameHouse\BattleShip\Game\Event\GameEvent;

interface GameEventSerializerInterface
{
    /**
     * @param PlayerInterface $player
     * @param $input
     * @return GameEvent
     */
    public function deserialize(PlayerInterface $player, $input);

    public function serialize(GameEvent $input);
}