<?php

namespace GameHouse\BattleShip\Game;

class Board
{
    const MIN_COLUMN = 1;
    const MAX_COLUMN = 10;
    const MIN_ROW = 1;
    const MAX_ROW = 10;

    /**
     * @var BoardCell[][]
     */
    private $cells;
    private $hash;

    public function __construct()
    {
        $this->cells = array();

        foreach (range(static::MIN_COLUMN, static::MAX_COLUMN) as $col) {
            $this->cells[$col] = array();
            foreach (range(static::MIN_ROW, static::MAX_ROW) as $row) {
                $this->cells[$col][$row] = new BoardCell(BoardCell::WATER);
            }
        }
    }

    /**
     * Set reported hash
     *
     * @param $hash
     */
    public function setHash($hash)
    {
        if ($this->hash !== null && $this->hash !== $hash) {
            throw new \InvalidArgumentException('BoardHash has been already set');
        }

        $this->hash = $hash;
    }

    /**
     * Get reported hash
     *
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Calculate real hash
     *
     * @return string
     */
    public function getRealHash()
    {
        $plain = $this->asString();

        return hash('sha256', $plain);
    }

    /**
     * @param Position $position
     *
     * @return BoardCell
     */
    public function getBoardCell(Position $position)
    {
        if (($position->getCol() < static::MIN_COLUMN) || ($position->getCol() > static::MAX_COLUMN)) {
            throw new \InvalidArgumentException(sprintf('Invalid column %d', $position->getCol()));
        }

        if (($position->getRow() < static::MIN_ROW) || ($position->getRow() > static::MAX_ROW)) {
            throw new \InvalidArgumentException(sprintf('Invalid row %d', $position->getRow()));
        }

        return $this->cells[$position->getCol()][$position->getRow()];
    }

    /**
     * Get board representation as plain string
     *
     * @return string
     */
    public function asString()
    {
        $string = '';

        foreach (range(static::MIN_ROW, static::MAX_ROW) as $row) {
            foreach (range(static::MIN_COLUMN, static::MAX_COLUMN) as $col) {
                $string .= $this->cells[$col][$row]->getContent();
            }
            $string .= ';';
        }

        return $string;
    }

    /**
     * Create a new board from given plain string
     *
     * @param $string
     * @return Board
     */
    public static function createFromString($string)
    {
        $expected = (Board::MAX_COLUMN - Board::MIN_COLUMN + 1 + 1) * (Board::MAX_ROW - Board::MIN_ROW + 1);
        if (strlen($string) !== $expected) {
            throw new \InvalidArgumentException('Bad board string, expected ' . $expected . ' but we get ' . strlen($string) . ' bytes : ' . $string);
        }

        $board = new Board();
        $board->cells = [];
        $col = Board::MIN_COLUMN;
        $row = Board::MIN_ROW;
        $array = str_split($string);

        foreach ($array as $char) {
            if ($char === ';') {
                if ($col !== Board::MAX_COLUMN + 1) {
                    throw new \InvalidArgumentException('Wrong column count at row number ' . $row . ': ' . $col);
                }
                $row++;
                $col = Board::MIN_COLUMN;
            } else {
                if ($row === Board::MIN_ROW) {
                    $board->cells[$col] = [];
                }
                $board->cells[$col][$row] = new BoardCell($char);
                $col++;
            }
        }

        if ($row !== Board::MAX_ROW + 1) {
            throw new \InvalidArgumentException('Wrong row count: ' . $row);
        }

        return $board;
    }

    /// TODO Move to other class
    public static function checkEquivalent(Board $board, ShipsLibrary $library, Board $other, ShipsLibrary $otherLib)
    {
        foreach (range(static::MIN_COLUMN, static::MAX_COLUMN) as $col) {
            foreach (range(static::MIN_ROW, static::MAX_ROW) as $row) {
                $position = new Position($col, $row);
                $boardCell = $board->getBoardCell($position);
                $otherCell = $other->getBoardCell($position);

                if ($boardCell->isVisible()) {
                    $boardContent = $boardCell->getContent() === BoardCell::WATER ?
                        'water' :
                        $library->findById($boardCell->getContent())->getName();

                    $otherContent = $otherCell->getContent() === BoardCell::WATER ?
                        'water' :
                        $otherLib->findById($otherCell->getContent())->getName();

                    if ($boardContent !== $otherContent) {
                        echo 'Non matching visible cell on ' . $position->asString() . ' ' . $boardContent . ' != ' . $otherContent . PHP_EOL;
                        return false;
                    }
                }
            }
        }

        return true;
    }
}