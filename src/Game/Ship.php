<?php

namespace GameHouse\BattleShip\Game;

class Ship
{
    private $name;
    private $size;
    private $id;
    private $publicId;

    public function __construct($id, $publicId, $size, $name)
    {
        $this->name = $name;
        $this->size = $size;
        $this->publicId = $publicId;
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getId()
    {
        return $this->id;
    }
}