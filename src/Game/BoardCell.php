<?php

namespace GameHouse\BattleShip\Game;


class BoardCell
{
    const WATER = '~';

    protected $content;
    protected $visible;

    public function __construct($content, $visible = false)
    {
        if (!is_string($content) || count($content) !== 1) {
            throw new \InvalidArgumentException('BoardCell content must be one character');
        }

        $this->content = $content;
        $this->visible = $visible;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content, $force = false) {
        if ($this->content !== static::WATER && !$force) {
            throw new \InvalidArgumentException('BoardCell already with content: ' . $this->content);
        }

        $this->content = $content;
    }

    public function isVisible()
    {
        return $this->visible;
    }

    public function setVisible($visible = true)
    {
        $this->visible = $visible;
    }
}