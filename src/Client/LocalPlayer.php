<?php

namespace GameHouse\BattleShip\Client;

use Bart\EscapeColors;
use Clue\React\Stdio\Stdio;
use GameHouse\BattleShip\Game\Board;
use GameHouse\BattleShip\Game\BoardCell;
use GameHouse\BattleShip\Game\Event\Error;
use GameHouse\BattleShip\Game\Event\GameEvent;
use GameHouse\BattleShip\Game\Event\GameResult;
use GameHouse\BattleShip\Game\Event\GameStateEvent;
use GameHouse\BattleShip\Game\Event\FireShoot;
use GameHouse\BattleShip\Game\Event\ResponseBoard;
use GameHouse\BattleShip\Game\Event\ResponseShoot;
use GameHouse\BattleShip\Game\Game;
use GameHouse\BattleShip\Game\GameEventSerializerInterface;
use GameHouse\BattleShip\Game\Player;
use GameHouse\BattleShip\Game\Position;
use GameHouse\BattleShip\Game\Ship;
use React\EventLoop\LibEventLoop;

class LocalPlayer extends Player
{
    /** @var LibEventLoop */
    private $eventLoop;

    /** @var GameEventSerializerInterface */
    private $builder;

    /** @var Stdio */
    private $stdio;

    /** @var ClientUI */
    private $ui;

    protected $placedShips = [];


    public function __construct($eventLoop, Stdio $stdio, GameEventSerializerInterface $builder)
    {
        parent::__construct();
        $this->eventLoop = $eventLoop;
        $this->stdio = $stdio;
        $this->builder = $builder;
    }

    public function setGame(Game $game)
    {
        parent::setGame($game);
        if ($this->ui === null) {
            $this->ui = new ClientUI($this, $this->getGame(), $this->getStdio());
        }
    }

    public function notify(GameEvent $event)
    {
        $this->stdio->writeln('Received ' . $this->builder->serialize($event));

        /* Extract to LocalPlayer */
        if ($event instanceof FireShoot) {
            $cell = $this->getBoard()->getBoardCell($event->getPosition());

            // Mark cell as visible
            $cell->setVisible();

            if ($cell->getContent() === BoardCell::WATER) {
                $response = new ResponseShoot($this->getId(), $event->getPosition(), 'miss');
            } else {
                $library = $this->getGame()->getShipLibrary();
                $ship = $library->findById($cell->getContent());
                $sink = $this->checkForSunk($cell->getContent());
                $response = new ResponseShoot($this->getId(), $event->getPosition(), 'hit', $ship->getName(), $sink);
            }
            $this->sendEventDelayed($response);
        }

        if ($event instanceof Error) {
            $this->stdio->writeln(EscapeColors::fg_color('red', $event->getMessage()));
        }

        if ($event instanceof ResponseShoot || $event instanceof FireShoot || $event instanceof GameStateEvent || $event instanceof ResponseBoard) {
            $this->printBoard();
        }

        if ($event instanceof GameResult) {
            $this->stdio->writeln($event->getMessage());
        }
    }

    public function shutdown()
    {
        if ($this->stdio !== null) {
            $this->stdio->end();
        }
    }

    public function placeShip($id, Position $cell, $orientation, $size)
    {
        $shipName = $id;
        if ($id instanceof Ship) {
            $shipName = $id->getName();
            $id = $id->getId();
        }
        if (in_array($id, $this->placedShips)) {
            throw new \InvalidArgumentException(sprintf('Ship %s already placed', $shipName));
        }

        $board = $this->getBoard();
        foreach (range(0, $size - 1) as $i) {
            $test = null;
            if (mb_strtoupper($orientation) === 'H') {
                $test = new Position($cell->getCol() + $i, $cell->getRow());
            } else {
                $test = new Position($cell->getCol(), $cell->getRow() + $i);
            }

            if (!$test->isValid()) {
                throw new \InvalidArgumentException('Out of boards');
            }

            $current = $board->getBoardCell($test);
            if ($current->getContent() !== BoardCell::WATER) {
                throw new \InvalidArgumentException('Ship overlaps');
            }
        }

        // Place ship
        foreach (range(0, $size - 1) as $pos) {
            $col = $cell->getCol();
            $row = $cell->getRow();
            if (mb_strtoupper($orientation) === 'H') {
                $col += $pos;
            } else {
                $row += $pos;
            }
            $board->getBoardCell(new Position($col, $row))->setContent($id);
        }

        $this->placedShips[] = $id;
    }

    protected function checkForSunk($shipId)
    {
        $hidden = 0;
        foreach(range(Board::MIN_COLUMN, Board::MAX_COLUMN) as $col) {
            foreach(range(Board::MIN_ROW, Board::MAX_ROW) as $row) {
                $cell = $this->getBoard()->getBoardCell(new Position($col, $row));
                if (($cell->isVisible() === false) && ($cell->getContent() === $shipId)) {
                    $hidden++;
                }
            }
        }

        return $hidden === 0;
    }

    public function haveBoats()
    {
        foreach($this->getGame()->getShipLibrary()->getShips() as $ship) {
            if ($this->checkForSunk($ship->getId()) !== true) {
                return true;
            }
        }

        return false;
    }

    protected function sendEvent(GameEvent $event)
    {
        $this->getGame()->receiveEvent($this, $event);
    }

    protected function sendEventDelayed(GameEvent $event)
    {
        $player = $this;
        $this->eventLoop->addTimer(0.005, function () use ($player, $event) {
            $this->getGame()->receiveEvent($this, $event);
        });
    }

    protected function printBoard()
    {
        if ($this->ui) {
            $this->ui->printBoard();
        }
    }

    protected function getStdio()
    {
        return $this->stdio;
    }
}