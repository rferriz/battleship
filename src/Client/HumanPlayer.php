<?php

namespace GameHouse\BattleShip\Client;

use Bart\EscapeColors;
use Clue\React\Stdio\Stdio;
use GameHouse\BattleShip\Game\Board;
use GameHouse\BattleShip\Game\Event\GameEvent;
use GameHouse\BattleShip\Game\Event\Identification;
use GameHouse\BattleShip\Game\Event\LeaveGame;
use GameHouse\BattleShip\Game\Event\RequestBoard;
use GameHouse\BattleShip\Game\Event\RequestBoardHash;
use GameHouse\BattleShip\Game\Event\FireShoot;
use GameHouse\BattleShip\Game\Event\ResponseBoardHash;
use GameHouse\BattleShip\Game\GameEventSerializerInterface;
use GameHouse\BattleShip\Game\GameState;
use GameHouse\BattleShip\Game\Position;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

class HumanPlayer extends LocalPlayer
{
    use LoggerAwareTrait;

    public function __construct($eventLoop, Stdio $stdio, GameEventSerializerInterface $builder)
    {
        parent::__construct($eventLoop, $stdio, $builder);
        $this->logger = new NullLogger();

        $stdio->getReadline()->setPrompt('Your command > ');
        $stdio->on('line', [$this, 'onLine']);
    }

    public function onLine($data)
    {
        $args = explode(' ', $data);

        if (count($args) === 0) {
            $action = 'help';
        } else {
            $action = array_shift($args);
        }

        try {
            if ($action === 'name') {
                $name = $args[0];
                $this->setName($name);
                $this->sendEvent(new Identification($this->getId(), $name));
                $this->getStdio()->getReadline()->setPrompt($name . ' > ');

            } else if ($action === 'random') {
                $this->placeShips();
                $this->sendEvent(new ResponseBoardHash($this->getId(), $this->getBoard()->getRealHash()));

            } else if ($action === 'req_hash') {
                $this->sendEvent(new RequestBoardHash($this->getId()));

            } else if ($action === 'req_board') {
                $this->sendEvent(new RequestBoard($this->getId()));

            } else if ($action === 'leave') {
                $this->sendEvent(new LeaveGame($this->getId()));
                $this->getStdio()->end();

            } else if ($action === 'fire') {
                $position = Position::createFromString($args[0]);
                $this->sendEvent(new FireShoot($this->getId(), $position));

            } else if ($action === 'draw') {
                $this->printBoard();
            } else if ($action === 'place') {
                if ($this->getGame()->getState() !== GameState::SETUP) {
                    throw new \InvalidArgumentException('Game already started.');
                }

                try {
                    $ship = $this->getGame()->getShipLibrary()->findByName($args[0]);
                    $position = Position::createFromString($args[1]);
                    $orientation = $args[2];

                    $this->placeShip($ship, $position, $orientation, $ship->getSize());

                    $this->onLine('draw');

                    if (count($this->placedShips) === count($this->getGame()->getShipLibrary()->getShips())) {
                        $this->sendEvent(new ResponseBoardHash($this->getId(), $this->getBoard()->getRealHash()));
                    }
                } catch (\Exception $e) {
                    $this->getStdio()->writeln(EscapeColors::fg_color('blue', sprintf("Ships in dock\n%s",$this->pendingShipsToPlaceStr())));
                    throw $e;
                }

            } else {
                $action = 'help';
            }

            if ($action === 'help') {
                $help = 'Commands .' . "\n";
                if ($this->getGame()->getState() === GameState::INVALID) {
                    $help .=
                        ' name     your_name' . "\n" .
                        ' random   Place ships randomly' . "\n";
                }

                if ($this->getGame()->getState() === GameState::SETUP) {
                    $help .=
                        ' name your_name      Set your name ' . "\n" .
                        ' random              Place ships randomly' . "\n" .
                        ' req_hash            Request opponent hash' . "\n" .
                        ' draw                Draw User Interface' . "\n" .
                        ' place ship x:y h/v  Place ship [ship] starting on position [x:y] oriented [h]orizontal or [v]ertical' ."\n";
                }

                if ($this->getGame()->getState() === GameState::ACTIVE_SHOULD_FIRE) {
                    $help .=
                        ' fire row:col    Fire at position row:col' . "\n" .
                        ' surrender       Surrender the game' . "\n";
                }

                if ($this->getGame()->getState() === GameState::PASSIVE_SHOULD_REPORT) {
                    $help .=
                        ' surrender       Surrender the game' . "\n";
                }

                if ($this->getGame()->getState() === GameState::ANTI_CHEAT) {
                    $help .=
                        ' surrender       Surrender the game' . "\n";
                }

                $help .= ' leave           Leave game';

                $this->getStdio()->writeln($help);
            }
        } catch (\InvalidArgumentException $e) {
            $this->getStdio()->writeln(EscapeColors::fg_color('red', $e->getMessage()));
        }
    }

    private function placeShips()
    {
        foreach ($this->getGame()->getShipLibrary()->getShips() as $ship) {
            $this->logger->info('Place ship ' . $ship->getName() . ' ' . $ship->getId() . ' ' . $ship->getSize());
            while (true) {
                try {
                    $cell = new Position(mt_rand(Board::MIN_COLUMN, Board::MAX_COLUMN), mt_rand(Board::MIN_ROW, Board::MAX_ROW));
                    $this->placeShip($ship->getId(), $cell, mt_rand(0, 1) ? 'H' : 'V', $ship->getSize());
                    break;
                } catch (\InvalidArgumentException $e) {
                    $this->logger->error('error'.$e->getMessage());
                }
            }
        }

        $hash = $this->getBoard()->getRealHash();
        $this->getBoard()->setHash($hash);
    }

    private function pendingShipsToPlaceStr() {
        $all = $this->getGame()->getShipLibrary()->getShips();
        $str = '';
        foreach($all as $ship) {
            if (!in_array($ship->getId(), $this->placedShips)) {
                $str .= sprintf(" %10s %d\n", $ship->getName(), $ship->getSize());
            }
        }

        return $str;
    }
}