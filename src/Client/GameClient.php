<?php

namespace GameHouse\BattleShip\Client;

use Clue\React\Stdio\Stdio;
use GameHouse\BattleShip\Game\Game;
use GameHouse\BattleShip\Game\GameEventSerializer;
use GameHouse\BattleShip\Game\GameEventValidator;
use GameHouse\BattleShip\Game\Players;
use GameHouse\BattleShip\Game\ShipsLibrary;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use React\EventLoop\Factory;
use React\Promise\Deferred;
use React\Socket\ConnectionInterface;
use React\Socket\Connector;

class GameClient
{
    use LoggerAwareTrait;

    private $eventLoop;
    /** @var Connector */
    private $connector;
    /** @var RemotePlayer */
    private $remote;
    /** @var LocalPlayer */
    private $local;
    /** @var Stdio */
    private $stdio;
    private $serializer;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->eventLoop = Factory::create();
        $this->stdio = new Stdio($this->eventLoop);
        $this->serializer = new GameEventSerializer();

        $dnsResolverFactory = new \React\Dns\Resolver\Factory();
        $dns = $dnsResolverFactory->createCached('8.8.8.8', $this->eventLoop);
        $this->connector = new Connector($this->eventLoop, [
            'dns' => $dns,
        ]);
    }

    public function setupPlayer($option)
    {
        if ($option !== null) {
            $ai = \GameHouse\BattleShip\Game\AI\Factory::create($option);
            return new ComputerPlayer($this->eventLoop, $this->stdio, $this->serializer, $ai);
        }
        return new HumanPlayer($this->eventLoop, $this->stdio, $this->serializer);
    }

    public function start($uri, $option)
    {
        $this->local = $this->setupPlayer($option);

        $client = $this;
        $this->connector->connect($uri)->then(function (ConnectionInterface $connection) use ($client) {
            $client->remote = new RemotePlayer($connection, $this->serializer);
            $client->remote->setLogger($this->logger);

            $players = new Players($this->remote, $this->local);
            $library = new ShipsLibrary();
            $eventValidator = new GameEventValidator($players);

            $deferred = new Deferred();
            $deferred->promise()->then(function ($results) use ($client) {
                $this->eventLoop->addTimer(1, function () use ($client) {
                    $client->local->shutdown();
                    $client->remote->getConnection()->close();
                    $client->eventLoop->stop();
                });
            });

            $game = new Game($players, $library, $eventValidator, $this->logger);
            $game->start($deferred);

        });

        $this->eventLoop->run();
    }
}