<?php

namespace GameHouse\BattleShip\Client;


use Bart\EscapeColors;
use Clue\React\Stdio\Stdio;
use GameHouse\BattleShip\Game\Board;
use GameHouse\BattleShip\Game\BoardCell;
use GameHouse\BattleShip\Game\Game;
use GameHouse\BattleShip\Game\PlayerInterface;
use GameHouse\BattleShip\Game\Position;
use GameHouse\BattleShip\Game\ShipsLibrary;

class ClientUI
{
    /**
     * @var PlayerInterface
     */
    private $player;

    /**
     * @var Game
     */
    private $game;

    private $stdio;

    public function __construct(PlayerInterface $player, Game $game, Stdio $stdio)
    {
        $this->player = $player;
        $this->game = $game;
        $this->stdio = $stdio;
    }

    public function printBoard()
    {
        $opponent = $this->game->getPlayers()->getOpponent($this->player);
        $myBoard = $this->player->getBoard();
        $opBoard = $opponent->getBoard();

        $this->stdio->writeln('Current Hash ' . $myBoard->getHash());
        $this->stdio->writeln('Opponent Hash ' . $opBoard->getHash());
        $this->stdio->writeln(sprintf('Game status: %-30s   Active player: %10s', $this->game->getState(), $this->game->getPlayers()->getActive()->getName()));
        $this->stdio->writeln($this->printBoardHeader());
        foreach (range(Board::MIN_ROW, Board::MAX_ROW) as $row) {
            $ownRow = $this->getRow($myBoard, $row, $this->player->getLibrary());
            $oppRow = $this->getRow($opBoard, $row, $opponent->getLibrary());
            $this->stdio->writeln($ownRow . '  |  ' . $oppRow);
        }
    }

    private function getRow(Board $board, $row, ShipsLibrary $ships)
    {
        $text = '';

        foreach (range(Board::MIN_COLUMN, Board::MAX_COLUMN) as $col) {
            $position = new Position($col, $row);
            if ($col === Board::MIN_COLUMN) {
                $text .= sprintf(' %s ', $position->getRowAsString());
            }

            $cell = $board->getBoardCell($position);
            $character = $cell->getContent();
            $color = 'blue';
            try {
                if ($character === BoardCell::WATER) {
                    if ($cell->isVisible()) {
                        $color = 'bold_blue';
                    }
                } else {
                    $color = 'dark_gray';
                    if ($cell->isVisible()) {
                        $ship = $ships->findById($character);
                        $color = 'white';
                        switch ($ship->getName()) {
                            case 'carrier':
                                $color = 'red';
                                break;
                            case 'battleship':
                                $color = 'yellow';
                                break;
                            case 'submarine':
                                $color = 'purple';
                                break;
                            case 'cruiser':
                                $color = 'green';
                                break;
                            case 'destroyer':
                                $color = 'cyan';
                                break;
                        }
                    }
                }

                $text .= ' ' . EscapeColors::fg_color($color, $character) . '';
            } catch (\Exception $e) {
                $this->stdio->writeln(EscapeColors::fg_color('red', $e->getMessage()));
            }
        }

        return $text;
    }

    private function printBoardHeader()
    {
        // Print header
        $text = '   ';
        foreach (range(Board::MIN_COLUMN, Board::MAX_COLUMN) as $i) {
            $position = new Position($i, Board::MIN_ROW);
            //$text .= sprintf('%2s |', $position->getColAsString());
            $text .= sprintf('%2s', $position->getColAsString());
        }

        $text .= '  |     ';
        foreach (range(Board::MIN_COLUMN, Board::MAX_COLUMN) as $i) {
            $position = new Position($i, Board::MIN_ROW);
            //$text .= sprintf('%2s |', $position->getColAsString());
            $text .= sprintf('%2s', $position->getColAsString());
        }

        return $text;
    }

}