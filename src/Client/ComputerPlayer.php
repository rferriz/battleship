<?php

namespace GameHouse\BattleShip\Client;

use Clue\React\Stdio\Stdio;
use GameHouse\BattleShip\Game\AI\AIInterface;
use GameHouse\BattleShip\Game\Board;
use GameHouse\BattleShip\Game\Event\GameEvent;
use GameHouse\BattleShip\Game\Event\Identification;
use GameHouse\BattleShip\Game\Event\FireShoot;
use GameHouse\BattleShip\Game\Event\ResponseBoardHash;
use GameHouse\BattleShip\Game\Event\ResponseShoot;
use GameHouse\BattleShip\Game\Event\Surrender;
use GameHouse\BattleShip\Game\GameEventSerializerInterface;
use GameHouse\BattleShip\Game\GameState;
use GameHouse\BattleShip\Game\Position;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use React\EventLoop\LibEventLoop;

class ComputerPlayer extends LocalPlayer
{
    use LoggerAwareTrait;

    private $nameSent;
    private $boardHashSent;
    private $ai;

    /**
     * ComputerPlayer constructor.
     * @param LibEventLoop $eventLoop
     * @param Stdio $stdio
     * @param GameEventSerializerInterface $builder
     * @param AIInterface $ai
     */
    public function __construct($eventLoop, Stdio $stdio, GameEventSerializerInterface $builder, AIInterface $ai)
    {
        parent::__construct($eventLoop, $stdio, $builder);
        $this->logger = new NullLogger();
        $this->ai = $ai;

        $stdio->getReadline()->setPrompt('Your command > ');
        $eventLoop->addPeriodicTimer(0.05, [$this, 'timerTick']);
    }

    public function notify(GameEvent $event)
    {
        parent::notify($event);

        if ($event instanceof ResponseShoot) {
            $this->ai->handleResponseShoot($event);
        }
    }

    public function timerTick()
    {
        //$this->getStdio()->writeln('TICK ' . $this->getGame()->getState() . ' ' . $this->getName() . ' ' . $this->getGame()->getPlayers()->getActive()->getName() . ' ' . $this->getGame()->getPlayers()->getPassive()->getName());
        $game = $this->getGame();

        if ($this->nameSent !== true) {
            $this->getStdio()->writeln('Sending name');
            $this->nameSent = true;
            $this->setName('Computer' . mt_rand());
            $this->sendEvent(new Identification($this->getId(), 'Computer' . mt_rand()));
        } else if ($this->boardHashSent !== true) {
            $this->ai->setup($this);
            $this->ai->placeShips($this);
            $this->getStdio()->writeln('Sending board hash');
            $this->boardHashSent = true;
            $this->sendEvent(new ResponseBoardHash($this->getId(), $this->getBoard()->getRealHash()));
        } else if ($game->isPlaying()) {

            if (!$this->haveBoats()) {
                $this->sendEvent(new Surrender($this->getId()));
            } else if ($game->getPlayers()->getActive()->getName() === $this->getName()) {
                if ($this->getGame()->getState() === GameState::ACTIVE_SHOULD_FIRE) {
                    $this->prepareShoot();
                }
            }
        } else if ($game->isAntiCheat()) {
            // Just wait for GameResult event
        }
    }

    protected function prepareShoot()
    {
        $position = $this->ai->getTargetPosition();

        if ($position === null) {
            $position = new Position(mt_rand(Board::MIN_COLUMN, Board::MAX_COLUMN), mt_rand(Board::MIN_ROW, Board::MAX_ROW));
        }

        $this->sendEventDelayed(new FireShoot($this->getId(), $position));
    }
}
