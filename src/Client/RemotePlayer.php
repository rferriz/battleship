<?php

namespace GameHouse\BattleShip\Client;

use GameHouse\BattleShip\Game\Event\Error;
use GameHouse\BattleShip\Game\Event\GameEvent;
use GameHouse\BattleShip\Game\Event\LeaveGame;
use GameHouse\BattleShip\Game\Game;
use GameHouse\BattleShip\Game\GameEventSerializerInterface;
use GameHouse\BattleShip\Game\Player;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use React\Socket\ConnectionInterface;

class RemotePlayer extends Player
{
    use LoggerAwareTrait;

    /** @var ConnectionInterface */
    private $connection;
    /** @var string */
    private $buffer;
    /** @var GameEventSerializerInterface */
    private $builder;

    /** @var array|string[] */
    private $lastsMessages;

    public function __construct(ConnectionInterface $connection, GameEventSerializerInterface $builder)
    {
        parent::__construct();
        $this->connection = $connection;
        $this->buffer = '';
        $this->builder = $builder;
        $this->logger = new NullLogger();
        $this->connection->on('data', [$this, 'onData']);
        $this->lastsMessages = [];
        $player = $this;
        $this->connection->on('close', function () use ($player) {
            if ($player->getGame() !== null) {
                $player->getGame()->receiveEvent($player, new LeaveGame($player->getId()));
            }
        });
    }

    public function setGame(Game $game)
    {
        parent::setGame($game);
        $this->onData('');
    }

    public function notify(GameEvent $event)
    {
        $message = $this->builder->serialize($event);

        // Avoid send duplicate messages
        if (in_array($message, $this->lastsMessages)) {
            return;
        }
        $this->lastsMessages[] = $message;

        $this->logger->debug('Sending to ' . (string)$this . ': ' . $message);

        $this->connection->write($message . PHP_EOL);

        if (count($this->lastsMessages) > 10) {
            array_shift($this->lastsMessages);
        }
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function onData($data)
    {
        $this->buffer .= $data;

        if ($this->getGame() === null)
            return;

        if (strpos($this->buffer, PHP_EOL) !== false) {
            $chunks = explode(PHP_EOL, $this->buffer);
            $this->buffer = array_pop($chunks);

            foreach ($chunks as $message) {
                $this->logger->debug('Received from ' . (string)$this . ': ' . $message);

                try {
                    $event = $this->builder->deserialize($this, $message);

                    $this->getGame()->receiveEvent($this, $event);

                } catch (\InvalidArgumentException $e) {
                    $error = new Error($this->getId(), $e->getMessage());
                    $this->notify($error);
                }
            }
        }
    }
}