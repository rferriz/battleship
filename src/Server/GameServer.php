<?php

namespace GameHouse\BattleShip\Server;

use GameHouse\BattleShip\Client\RemotePlayer;
use GameHouse\BattleShip\Game\Game;
use GameHouse\BattleShip\Game\GameEventSerializer;
use GameHouse\BattleShip\Game\GameEventValidator;
use GameHouse\BattleShip\Game\PlayerInterface;
use GameHouse\BattleShip\Game\Players;
use GameHouse\BattleShip\Game\ShipsLibrary;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use React\EventLoop\Factory;
use React\Promise\Deferred;
use React\Socket\ConnectionInterface;
use React\Socket\Server;

class GameServer
{
    use LoggerAwareTrait;

    private $eventLoop;
    /** @var Server */
    private $socket;
    /** @var array|RemotePlayer[] */
    private $players = [];

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function start($port = 21080)
    {
        $this->eventLoop = Factory::create();
        $this->socket = new Server('0.0.0.0:' . $port, $this->eventLoop);
        $this->setup();
        $this->logger->info('Server started at ' . $this->socket->getAddress());
        $this->eventLoop->run();
    }

    private function setup()
    {
        $server = $this;
        $this->socket->on('connection', function (ConnectionInterface $connection) use ($server) {
            $server->logger->info('Received connection from ' . $connection->getRemoteAddress());

            if (count($server->players) < 2) {
                //$player = new PlayerConnection($connection);
                $player = new RemotePlayer($connection, new GameEventSerializer());
                $player->setLogger($server->logger);
                $server->addPlayer($player);
            }

            if (count($server->players) === 2)
                $server->setupGame();
        });
    }

    private function setupGame()
    {
        $this->logger->info('New game');
        $players = new Players(array_shift($this->players), array_shift($this->players));
        $library = new ShipsLibrary();
        $eventValidator = new GameEventValidator($players);

        $deferred = new Deferred();
        $deferred->promise()->then(function ($results) use ($players) {
            $serializer = new GameEventSerializer();
            foreach ($results as $result) {
                foreach ($players->getPlayers() as $player) {
                    if ($player->getName() === $result['player'] && $player instanceof RemotePlayer) {
                        if (array_key_exists('event', $result)) {
                            $player->getConnection()->write($serializer->serialize($result['event']).PHP_EOL);
                        }

                        // Give some time to the loop to send all packets
                        $this->eventLoop->addTimer(1, function () use ($player) {
                            $player->getConnection()->close();
                        });

                    }
                }
            }
            $this->eventLoop->addTimer(1, function () {
                echo 'GAME OVER' . PHP_EOL . PHP_EOL;
            });
        });

        $game = new Game($players, $library, $eventValidator, $this->logger);
        $game->isServer(true);
        $game->start($deferred);
    }

    private function addPlayer(PlayerInterface $player)
    {
        $this->players[] = $player;
    }
}